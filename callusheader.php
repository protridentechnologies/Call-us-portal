<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Call-Us | Home</title>
    
    <!-- Font awesome -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">   
    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="css/jquery.smartmenus.bootstrap.css" rel="stylesheet">
    <!-- Product view slider -->
    <link rel="stylesheet" type="text/css" href="css/jquery.simpleLens.css">    
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="css/nouislider.css">
    <!-- Theme color -->
    <link id="switcher" href="css/theme-color/default-theme.css" rel="stylesheet">
    <!-- <link id="switcher" href="css/theme-color/bridge-theme.css" rel="stylesheet"> -->
    <!-- Top Slider CSS -->
    <link href="css/sequence-theme.modern-slide-in.css" rel="stylesheet" media="all">

    <!-- Main style sheet -->
    <link href="css/style.css" rel="stylesheet">    

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  
    
  </head>
  <body> 
   <!-- wpf loader Two -->
    <div id="wpf-loader-two">          
      <div class="wpf-loader-two-inner">
        <span>Loading</span>
      </div>
    </div> 
    <!-- / wpf loader Two -->       
  <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <!-- Start header section -->
  <header id="aa-header">
    <!-- start header top  -->
    <div class="aa-header-top">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="aa-header-top-area">
              <!-- start header top left -->
              <div class="aa-header-top-left">
                <!-- start language -->
                <!--<div class="aa-language">
                  <div class="dropdown">
                    <a class="btn dropdown-toggle" href="#" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      <img src="img/flag/english.jpg" alt="english flag">ENGLISH
                      <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                      <li><a href="#"><img src="img/flag/french.jpg" alt="">FRENCH</a></li>
                      <li><a href="#"><img src="img/flag/english.jpg" alt="">ENGLISH</a></li>
                    </ul>
                  </div>
                </div>-->
                <!-- / language -->
                <div class="cellphone ">
                  <p><span class="glyphicon glyphicon-envelope"></span>protriden@gmail.com</p>
                </div>
                <!-- start currency -->
                <!--<div class="aa-currency">
                  <div class="dropdown">
                    <a class="btn dropdown-toggle" href="#" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      <i class="fa fa-usd"></i>USD
                      <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                      <li><a href="#"><i class="fa fa-euro"></i>EURO</a></li>
                      <li><a href="#"><i class="fa fa-jpy"></i>YEN</a></li>
                    </ul>
                  </div>
                </div>-->
                <!-- / currency -->

                <style>
                  @media only screen and (max-width: 600px) {
                  .cat {
                 width: min-content;
                 font-size: smaller;
                }
                }
                </style>

                <!-- start cellphone -->
                <div class="cellphone hidden-xs">
                  <p><span class="fa fa-phone"></span>+91-98807675443</p>
                </div>
                <!-- / cellphone -->
              </div>
              <!-- / header top left --><!--class="hidden-xs"-->
              <div class="aa-header-top-right">
                <ul class="aa-head-top-nav-right">
                  <li class="hidden-xs cat"><a href="account.html">Account</a></li>
                  <li class="hidden-xs hidden-lg cat"><a href="wishlist.html">Wishlist</a></li>
                  <li class="hidden-xs cat"><a href="cart.html">Cart</a></li>
                  <li class=" cat"><a href="checkout.html">Checkout</a></li>
                  <li class="cat"><a href="" data-toggle="modal" data-target="#login-modal">Login</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- / header top  -->

    <!-- start header bottom  -->
    <div class="aa-header-bottom ">

      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="aa-header-bottom-area dog">
              
              <!-- logo  -->
              <div class="aa-logo hidden-xs">
                <!-- Text based logo -->
                <a href="index.html" >
                   <p> 
                  <p style="color: black;"  >Call<strong>Us</strong> <span style="font-weight: 900;" class="fgf" >Your needs at<br> your door step</span></p>
                  <img src="images/calluslogo.png"   alt="logo img" style="width: 100px;" >
                </a>
                <!-- img based logo -->
                <!-- <a href="index.html"><img src="img/logo.jpg" alt="logo img"></a> -->
              </div>
              <!-- / logo  -->


<!--mobile header start-->
              <style>
                .cards{
                  background-color:none;
                  min-height: 10px;
                  display: flex;
                 
                  justify-content: space-between;
              }
              .s{
                width: 10px;
                height: 20px;
              }
              </style>
              <div class="container  hidden-lg hidden-md hidden-sm cards">
              <!--xs logo   justify-content: space-between;-->
              <div>
              <div class="aa-logo hidden-lg hidden-md">
                <!-- Text based logo -->
                <a href="index.html" >
                   <p> 
                  <p style="color: black;" style="font-size: 10px;" >Call<strong>Us</strong> <span style="font-weight: 700; font-size: 5px;" >Your needs at<br> your door step</span></p>
                  <img src="images/calluslogo.png"   alt="logo img" style="width: 60px;" >
                </a>
              </div>
              </div>
              &nbsp;
              <!--/ xs logo-->

              <div >
                <div class="aa-cartbox hidden-lg hidden-md ">
                  <a class="aa-cart-link" href="#" style="width: 5px;" >
                    <span class="fa fa-search"></span>
                    <span class="aa-cart-title ">search</span>
                  </a>
                  </div>
                </div>

                &nbsp; 
                &nbsp;
                &nbsp;
              <div>
              <div class="aa-cartbox hidden-lg hidden-md">
                <a class="aa-cart-link" href="cart.html"  >
                  <span class="fa fa-shopping-cart" ></span>
                  <span class="aa-cart-title " >cart box</span>
                  <span class="aa-cart-notify" >2</span>
                </a>
                <div class="aa-cartbox-summary">
                  <ul>
                    <li>
                      <a class="aa-cartbox-img" href="#"><img src="images/cat/thump_1589022522.png" alt="img"></a>
                      <div class="aa-cartbox-info">
                        <h4><a href="#">Pizza</a></h4>
                        <p>1 x $250</p>
                      </div>
                      <a class="aa-remove-product" href="#"><span class="fa fa-times"></span></a>
                    </li>
                    <li>
                      <a class="aa-cartbox-img" href="#"><img src="images/cat/thump_1557138439.png" alt="img"></a>
                      <div class="aa-cartbox-info">
                        <h4><a href="#">Fruits</a></h4>
                        <p>1 x $250</p>
                      </div>
                      <a class="aa-remove-product" href="#"><span class="fa fa-times"></span></a>
                    </li>                    
                    <li>
                      <span class="aa-cartbox-total-title">
                        Total
                      </span>
                      <span class="aa-cartbox-total-price">
                        $500
                      </span>
                    </li>
                  </ul>
                  <a class="aa-cartbox-checkout aa-primary-btn" href="checkout.html">Checkout</a>
                </div>
                </div></div>

                <div >
                  <div class="aa-cartbox hidden-lg hidden-md ">
                    <a class="aa-cart-link" href="wishlist.html" style="width:5px;" >
                      <span class="fa fa-heart"></span>
                      <span class="aa-cart-title ">Wish</span>
                    </a>
                    </div>
                  </div>

                  
            </div>

<!--/ mobile header-->

               <!-- cart box -->
              <div class="aa-cartbox hidden-xs  ">
                <a class="aa-cart-link" href="#">
                  <span class="fa fa-shopping-cart"></span>
                  <span class="aa-cart-title ">Shopping-cart</span>
                  <span class="aa-cart-notify">2</span>
                </a>
                <div class="aa-cartbox-summary">
                  <ul>
                    <li>
                      <a class="aa-cartbox-img" href="#"><img src="images/cat/thump_1589022522.png" alt="img"></a>
                      <div class="aa-cartbox-info">
                        <h4><a href="#">Pizza</a></h4>
                        <p>1 x $250</p>
                      </div>
                      <a class="aa-remove-product" href="#"><span class="fa fa-times"></span></a>
                    </li>
                    <li>
                      <a class="aa-cartbox-img" href="#"><img src="images/cat/thump_1557138439.png" alt="img"></a>
                      <div class="aa-cartbox-info">
                        <h4><a href="#">Fruits</a></h4>
                        <p>1 x $250</p>
                      </div>
                      <a class="aa-remove-product" href="#"><span class="fa fa-times"></span></a>
                    </li>                    
                    <li>
                      <span class="aa-cartbox-total-title">
                        Total
                      </span>
                      <span class="aa-cartbox-total-price">
                        $500
                      </span>
                    </li>
                  </ul>
                  <a class="aa-cartbox-checkout aa-primary-btn" href="checkout.html">Checkout</a>
                </div>
              </div>
              <!-- / cart box -->
              <!-- xs cart box -->
              
              
              <!-- / xs cart box -->
            <!-- heart-->              
              <div class="aa-cartbox hidden-xs">
                <a class="aa-cart-link" href="wishlist.html">
                  <span class="glyphicon glyphicon-heart"></span>
                  <span class="aa-cart-title fgf">wishlist </span>
                  <span class="aa-cart-notify">8</span>
                </a>
                </div>
                <!--/ heart-->

              <!-- search box -->
              <div class="aa-search-box hidden-xs ">
                <form action="">
                  <input type="text" name="" id="" placeholder="Search here ex. 'man' ">
                  <button type="submit"><span class="fa fa-search"></span></button>
                </form>
              </div>
              <!-- / search box -->             
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- / header bottom  -->
  </header>
  <!-- / header section -->
  <!-- menu -->
  <section id="menu">
    <div class="container">
      <div class="menu-area">
        <!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>          
          </div>
          <div class="navbar-collapse collapse">
            <!-- Left nav -->
            <ul class="nav navbar-nav">
              <li><a href="index.html">Home</a></li>
              <li><a href="#">Groceries <span class="caret"></span></a>
                <ul class="dropdown-menu">                
                  <li><a href="#">Rice</a></li>
                  <li><a href="#">Wheat</a></li>
                  <li><a href="#">Black Grams</a></li>
                  <li><a href="#">Horse Grams</a></li>                                                
                  <li><a href="#">Biscuts</a></li>
                  <li><a href="#">Milks</a></li>
                  <li><a href="#">Chocklets</a></li>
                  <li><a href="#">groceries</a></li>
                  <li><a href="#">And more.. <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">groceries</a></li>
                      <li><a href="#">groceries</a></li>
                      <li><a href="#">groceries</a></li>                                      
                    </ul>
                  </li>
                </ul>
              </li>
              <li><a href="#">Fruits <span class="caret"></span></a>
                <ul class="dropdown-menu">  
                  <li><a href="#">Orange</a></li>                                                                
                  <li><a href="#">Strawberry</a></li>              
                  <li><a href="#">Mango</a></li>
                  <li><a href="#">Apple</a></li>
                  <li><a href="#">Banana</a></li>                
                  <li><a href="#">Cherry</a></li>
                  <li><a href="#">Goosberry</a></li>
                  <li><a href="#">And more.. <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Pears</a></li>
                      <li><a href="#">Jack fruit</a></li>
                      <li><a href="#">Grapes</a></li>
                      <li><a href="#">And more.. <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="#">fruits</a></li>
                          <li><a href="#">fruits</a></li>
                          <li><a href="#">fruits </a></li>
                          <li><a href="#">fruits</a></li>
                          <li class="disabled"><a class="disabled" href="#">Disabled item</a></li>                       
                          <li><a href="#">fruits</a></li>
                          <li><a href="#">fruits</a></li>
                          <li><a href="#">fruits</a></li>
                          <li><a href="#">fruits</a></li>
                          <li><a href="#">fruits</a></li>
                          <li><a href="#"> fruits</a></li>
                          <li><a href="#"> fruits</a></li>
                          <li><a href="#">fruits</a></li>
                          <li><a href="#"> fruits</a></li>
                          <li><a href="#"> fruits</a></li>
                          <li><a href="#"> fruits</a></li>
                          <li><a href="#"> fruits</a></li>
                          <li><a href="#"> fruits</a></li>                        
                          <li><a href="#">fruits</a></li>
                          <li><a href="#">fruits</a></li>                       
                        </ul>
                      </li>                   
                    </ul>
                  </li>
                </ul>
              </li>
              <li><a href="#">Vegetables <span class="caret"></span></a>
                <ul class="dropdown-menu">                
                  <li><a href="#">Carrot</a></li>
                  <li><a href="#">Tomato</a></li>
                  <li><a href="#">Onion</a></li>
                  <li><a href="#">Coriunder leaves</a></li>                                                
                  <li><a href="#">Cucumber</a></li>
                  <li><a href="#">Coconuts</a></li>
                  <li><a href="#">Tender coconut</a></li>
                  <li><a href="#">Potato</a></li>
                  <li><a href="#">And more.. <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#"> vegies</a></li>
                      <li><a href="#">vegies</a></li>
                      <li><a href="#">vegies</a></li>                                      
                    </ul>
                  </li>
                </ul>
              </li>
              <li><a href="#">Laundry</a></li>
             <li><a href="#">Pick-Up &drop <span class="caret"></span></a>
                <ul class="dropdown-menu">                
                  <li><a href="#">place</a></li>
                  <li><a href="#">place</a></li>
                  <li><a href="#">place</a></li>
                  <li><a href="#">place</a></li>                                                
                  <li><a href="#">place</a></li>                
                </ul>
              </li>
              <!--<li><a href="#">Furniture</a></li>            
              <li><a href="blog-archive.html">Blog <span class="caret"></span></a>
                <ul class="dropdown-menu">                
                  <li><a href="blog-archive.html">Blog Style 1</a></li>
                  <li><a href="blog-archive-2.html">Blog Style 2</a></li>
                  <li><a href="blog-single.html">Blog Single</a></li>                
                </ul>
              </li>-->
              <li><a href="contact.html">Contact</a></li>
              <li><a href="#">Pages <span class="caret"></span></a>
                <ul class="dropdown-menu">                
                  <li><a href="product.html">Shop Page</a></li>
                  <li><a href="product-detail.html">Shop Single</a></li>                
                  <li><a href="404.html">404 Page</a></li>                
                </ul>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>       
    </div>
  </section>
  <!-- / menu -->
</body>
</html>
